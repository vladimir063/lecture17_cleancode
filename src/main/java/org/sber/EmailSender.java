package org.sber;

public interface EmailSender {

  void sendHtmlSalaryReport(String recipients, StringBuilder resultingHtml);
}
