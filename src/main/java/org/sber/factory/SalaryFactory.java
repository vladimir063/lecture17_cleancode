package org.sber.factory;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.sber.domain.Salary;
import org.sber.domain.impl.MaxSalary;
import org.sber.domain.impl.MidSalary;
import org.sber.domain.impl.MinSalary;

public class SalaryFactory {

  public static Salary getSalary(ResultSet results) throws SQLException {
    double salary = results.getDouble("salary");
    if (salary > 200000) {
      return new MaxSalary(salary);
    }
    if (salary < 200000 * 0.1) {
      return new MidSalary(salary);
    }
    return new MinSalary(salary);
  }
}
