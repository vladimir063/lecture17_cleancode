package org.sber;

import java.sql.ResultSet;
import java.time.LocalDate;

public interface SqlGenerator {

  ResultSet getEmployeeInfo(String departmentId, LocalDate dateFrom, LocalDate dateTo);
}
