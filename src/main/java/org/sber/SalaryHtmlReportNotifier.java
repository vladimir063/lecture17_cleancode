package org.sber;

import java.time.LocalDate;

public interface SalaryHtmlReportNotifier {

  void generateAndSendReport(String departmentId, LocalDate dateFrom, LocalDate dateTo, String recipients);
}
