package org.sber.domain.impl;

import org.sber.domain.Salary;

public class MidSalary implements Salary {

  private final double salary;

  public MidSalary(double salary) {
    this.salary = salary;
  }

  @Override
  public String formattedSalary() {
    return "" + salary;
  }
}
