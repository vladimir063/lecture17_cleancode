package org.sber.domain.impl;

import org.sber.domain.Salary;

public class MaxSalary implements Salary {

  private final double salary;

  public MaxSalary(double salary) {
    this.salary = salary;
  }

  @Override
  public String formattedSalary() {
    return salary + " (taxes:" + salary * 0.2 + ")";
  }
}
