package org.sber.service.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import lombok.extern.slf4j.Slf4j;
import org.sber.SqlGenerator;

@Slf4j
public class SqlGeneratorImpl implements SqlGenerator {

  private final Connection connection;

  public SqlGeneratorImpl(Connection databaseConnection) {
    this.connection = databaseConnection;
  }

  @Override
  public ResultSet getEmployeeInfo(String departmentId, LocalDate dateFrom, LocalDate dateTo) {
    final String SQL = """
          select emp.id as emp_id, emp.name as amp_name, sum(salary) as salary 
          from employee emp 
          left join salary_payments sp on emp.id = sp.employee_id 
          where  emp.department_id = ? and sp.date >= ? and sp.date <= ? 
          group by emp.id, emp.name   
        """;

    try (PreparedStatement ps = connection.prepareStatement(SQL)) {
      ps.setString(0, departmentId);
      ps.setDate(1, new Date(dateFrom.toEpochDay()));
      ps.setDate(2, new Date(dateTo.toEpochDay()));
      return ps.executeQuery();
    } catch (SQLException e) {
      log.error("SQL Exception" + e);
      e.printStackTrace();
      throw new RuntimeException();
    }
  }
}
