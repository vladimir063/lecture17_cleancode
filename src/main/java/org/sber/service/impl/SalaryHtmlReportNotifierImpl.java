package org.sber.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import lombok.extern.slf4j.Slf4j;
import org.sber.EmailSender;
import org.sber.SalaryHtmlReportNotifier;
import org.sber.SqlGenerator;
import org.sber.factory.SalaryFactory;

@Slf4j
public class SalaryHtmlReportNotifierImpl implements SalaryHtmlReportNotifier {

  private final EmailSender email;
  private final SqlGenerator sqlGenerator;

  public SalaryHtmlReportNotifierImpl(EmailSender email, SqlGenerator sqlGenerator) {
    this.email = email;
    this.sqlGenerator = sqlGenerator;
  }

  public void generateAndSendReport(String departmentId, LocalDate dateFrom,
      LocalDate dateTo, String recipients) {

    StringBuilder resultingHtml = new StringBuilder();
    resultingHtml.append("<html><body><table><tr><td>Employee</td><td>Salary</td></tr>");
    double totals = 0;
    ResultSet employee = sqlGenerator.getEmployeeInfo(departmentId, dateFrom, dateTo);
    try {
      while (employee.next()) {
        //format salary and taxes
        String formattedSalary = SalaryFactory.getSalary(employee).formattedSalary();
        // process each row of query results
        resultingHtml.append("<tr>"); // add row start tag

        resultingHtml.append("<td>").append(employee.getString("emp_name")).append("</td>"); //         appending employee name
        resultingHtml.append("<td>").append(formattedSalary).append("</td>"); //    appending employee salary for period
        resultingHtml.append("</tr>"); // add row end tag
        totals += employee.getDouble("salary"); // add salary to totals
      }
    } catch (SQLException e) {
      log.error("SQL Exception" + e);
      e.printStackTrace();
    }
    resultingHtml.append("<tr><td>Total</td><td>").append(totals).append("</td></tr>");
    resultingHtml.append("</table></body></html>");
    // now when the report is built we need to send it to the recipients list
    email.sendHtmlSalaryReport(recipients, resultingHtml);
  }

}
