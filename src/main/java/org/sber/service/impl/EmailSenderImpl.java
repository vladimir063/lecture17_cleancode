package org.sber.service.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.sber.EmailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

@Slf4j
public class EmailSenderImpl implements EmailSender {

  @Override
  public void sendHtmlSalaryReport(String recipients, StringBuilder resultingHtml) {
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    // we're going to use google mail to send this message
    mailSender.setHost("mail.google.com");
    // construct the message
    MimeMessage message = mailSender.createMimeMessage();
    try {
      MimeMessageHelper helper = new MimeMessageHelper(message, true);
      helper.setTo(recipients);
      // setting message text, last parameter 'true' says that it is HTML format
      helper.setText(resultingHtml.toString(), true);
      helper.setSubject("Monthly department salary report");
      // send the message
      mailSender.send(message);
    } catch (MessagingException e) {
      log.error("Messaging Exception " + e);
      e.printStackTrace();
    }
  }
}
